# Kubernetes Workshop
Lab 03: Deploy and Upgrade a Single App

---

## Instructions

Switch to work in the lab folder
```
cd ~/ez-sela-kubernetes-labs/Lab-03
```

### Deploy application using deployments

 - List existing Deployments
```
kubectl get deployments
```

 - List existing Pods
```
kubectl get pods
```

 - Inspect the Deployment's definition
```
cat frontend-deployment.yaml
```

 - Create the Deployment's resource
```
kubectl apply -f frontend-deployment.yaml
```

 - List existing Deployments
```
kubectl get deployments
```

 - List existing Pods
```
kubectl get pods
```

 - Inspect the Pods' images
```
kubectl describe pods
```

### Rolling a Zero-Downtime update

 - Edit the exported definition file by changing the container image to refer it to a new version of the image
```
nano frontend-deployment.yaml
```
```
Action:
-----------
Replace: selaworkshops/sentiment-analysis-frontend:v1
With: selaworkshops/sentiment-analysis-frontend:v2
```

 - Apply your changes to start the rolling upgrade sequence:
```
kubectl apply -f frontend-deployment.yaml
```

 - We can check the status of the rollout using the following command:
```
kubectl rollout status deployment frontend
```

 - Inspect the pods' images
```
kubectl describe pods
```

### Rolling back to a previous state

 - Inspect the rollout history using the following command:
```
kubectl rollout history deployment frontend
```

 - Rollback to the first revision of the Deployment:
```
kubectl rollout undo deployment frontend --to-revision=1
```

 - We can check the status of the rollout using the following command:
```
kubectl rollout status deployment frontend
```

 - Inspect the pods' images
```
kubectl describe pods
```

### Cleanup

 - List all existing resources
```
kubectl get all
```

 - Delete all existing resources
```
kubectl delete all --all
```

 - List all existing resources
```
kubectl get all
```
