# Kubernetes Workshop
Lab 09: Deploying Prometheus using Helm

---

## Instructions

Switch to work in the lab folder
```
cd ~/ez-sela-kubernetes-labs/Lab-09
```

### Deploy Prometheus using Helm Packages

 - Deploy Prometheus
```
helm install <ID>-prometheus prometheus-community/prometheus
```

 - List existing pods
```
kubectl get pods
```

 - List existing services
```
kubectl get services
```

 - Update the service type
```
kubectl patch svc prometheus-server -p '{"spec": {"type": "LoadBalancer"}}'
```

 - List existing services (wait until the load balancer is created and gets an IP)
```
kubectl get svc --watch
```

 - Browse to prometheus and check the configured targets of the Kubernetes cluster (surprise, everything is already configured!)
```
http://<service-ip>/targets
```

### Cleanup

 - List existing resources
```
kubectl get all
```

 - Delete the installed chart
```
helm delete <ID>-prometheus
```

 - List existing resources
```
kubectl get all
```

 - Delete the existing resources:
```
kubectl delete all --all
```

 - List existing resources
```
kubectl get all
```