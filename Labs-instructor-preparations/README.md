# Kubernetes Workshop
Lab 01: Setting Up Your Workstation

---

## Labs Infrastructure

<img alt="labs-infrastructure" src="labs-infrastructure.png"  width="60%" height="60%">

## Guidelines
The instructor will demo this lab, since all students will use the same Kubernetes cluster.
In this stage, the instructor will ssh to the Bastion-VM.

## Instructions

 - Install 'kubectl' using snap
```
sudo snap install kubectl --classic
```

 - Ensure kubectl is installed and check its version
```
kubectl version
```

 - Download youre newly created Kubernetes cluster credentials and configure kubectl
```
gcloud container clusters get-credentials $(hostname) --zone $(gcloud compute instances list --filter="name=$(hostname)" --format "value(zone)") --project devops-course-architecture
```

- Define the Kubernetes default text editor as NANO 
```
echo "export KUBE_EDITOR=\"nano\"" >>  ~/.bash_profile
```

- Define auto completion and aliases for Kubectl 
```
echo 'source <(kubectl completion bash)' >>~/.bash_profile
echo 'alias k=kubectl' >>~/.bash_profile
echo 'complete -F __start_kubectl k' >>~/.bash_profile
source ~/.bash_profile
```

 - Inspect current context
```
kubectl config get-contexts
```

 - Inspect kubectl configuration file
```
cat ~/.kube/config
```

 - List cluster nodes
```
kubectl get nodes
```

 - Inspect node details
```
kubectl describe node <node-name>
```

 - Get all cluster resources
```
kubectl get all --all-namespaces
```

### Prepare cluster for LAB05 with ingress controller

 - Create a new namespace for the ingress controller
```
kubectl create namespace ingress-nginx
```

 - Deploy the required resources of the Ingress controller:
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.2.1/deploy/static/provider/cloud/deploy.yaml
```

 - Verify NGINX controller installation 
```
kubectl get pods -n ingress-nginx -l app.kubernetes.io/name=ingress-nginx --watch
```

 - Inspect the ingress controller Service
```
kubectl get svc ingress-nginx-controller -n ingress-nginx
```
### Install HELM client

 - Download Helm Client
```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
```

 - Install Helm Client
```
chmod 700 get_helm.sh
./get_helm.sh
```

### Prepare Prometheus repository for later Helm Package deployment

 -  Add stable helm charts default repository so we we will be able to search and install stable chart from it
 
 ```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add stable https://charts.helm.sh/stable
```

-  Update the HELM repositories 
```
helm repo update
```


