#!/bin/bash

username=$(cat userlist | tr 'A-Z'  'a-z')

password=<password>

for user in $username
do
       useradd -m -s /bin/bash $user
       echo $user:$password | chpasswd
done

echo "$(wc -l /tmp/userlist) users have been created"
tail -n$(wc -l /tmp userlist) /etc/passwd