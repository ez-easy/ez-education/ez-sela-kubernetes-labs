# Kubernetes Workshop
Lab 01: Setting Up Your Workstation

---

## Labs Infrastructure

<img alt="labs-infrastructure" src="labs-infrastructure.png"  width="60%" height="60%">

### Create a workspace for each student

The instructor will provide each student a unique namespace-identifier and the IP address of the bastion-VM along with the credentials for connecting it.

The student will perform the following steps:
- SSH to the bastion-VM using the provided credentials:
```
ssh <ID>@<bastion-VM ip-address>
```
- clone the labs repository
```
git clone https://gitlab.com/ez-mentor/ez-learn/ez-sela-kubernetes-labs
cd ez-sela-kubernetes-labs
```

 - Download youre newly created Kubernetes cluster credentials and configure kubectl
```
gcloud container clusters get-credentials $(hostname) --zone $(gcloud compute instances list --filter="name=$(hostname)" --format "value(zone)") --project devops-course-architecture
```

- Define the Kubernetes default text editor as NANO 
```
echo "export KUBE_EDITOR=\"nano\"" >>  ~/.bash_profile
```

- Define auto completion and aliases for Kubectl 
```
echo 'source <(kubectl completion bash)' >>~/.bash_profile
echo 'alias k=kubectl' >>~/.bash_profile
echo 'complete -F __start_kubectl k' >>~/.bash_profile
source ~/.bash_profile
```

 - Inspect current context
```
kubectl config get-contexts
```

 - Inspect kubectl configuration file
```
cat ~/.kube/config
```

- create a namespace for each student
```
kubectl create namespace <ID>
```

- set the namespace to be the default one
```
kubectl config set-context --current --namespace=<ID>
```

