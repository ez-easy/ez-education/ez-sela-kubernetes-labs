# Kubernetes Workshop
Lab 05: Deploying applications using Ingress

---

## Instructions

Switch to work in the lab folder
```
cd ~/ez-sela-kubernetes-labs/Lab-05
```

### Deploy the application

 - adjust the `cats.yaml` application-file (pod and ClusterIP service) to your ID
```
nano cats.yaml
```

 - Deploy the "**Cats**" application
```
kubectl create -f cats.yaml
```

 - adjust the `dogs.yaml` application-file (pod and ClusterIP service) to your ID
```
nano dogs.yaml
```

 - Deploy the "**Dogs**" application

```
kubectl create -f dogs.yaml
```

 - adjust the `birds.yaml` application-file (pod and ClusterIP service) to your ID
```
nano birds.yaml
```

 - Deploy the "**Birds**" application
```
kubectl create -f birds.yaml
```

 - adjust the `ingress.yaml` application-file (pod and ClusterIP service) to your ID
```
nano ingress.yaml
```

 - Create the ingress resource
```
kubectl create -f ingress.yaml
```

 - List existing pods
```
kubectl get pods
```

 - List existing services
```
kubectl get svc
```

 - List existing ingress 
```
kubectl get ingress
```

### Access the application

 - Get the ingress controller External IP 
```
kubectl get svc ingress-nginx-controller -n ingress-nginx
```

 - Browse to the cats service
```
http://<ingress-controller-external-ip>/<ID>/cats
```

 - Browse to the dogs service
```
http://<ingress-controller-external-ip>/<ID>/dogs
```

 - Browse to the birds service
```
http://<ingress-controller-external-ip>/<ID>/birds
```

### Cleanup

 - List existing resources
```
kubectl get all
```

 - Delete ingress resource
```
kubectl delete ingress app-ingress-<ID>
```

 - Delete existing resources
```
kubectl delete all --all
```

 - List existing resources
```
kubectl get all
```
