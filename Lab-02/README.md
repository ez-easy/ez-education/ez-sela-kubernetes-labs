# Kubernetes Workshop
Lab 02: Creating and scaling your first pod

---

## Instructions

Switch to work in the lab folder
```
cd ~/ez-sela-kubernetes-labs/Lab-02
```

### Creating Our First Pod

 - Inspect the Pod's definition
```
cat echoserver-pod.yaml
```

 - List current existing Pods
```
kubectl get pods
```

 - Create the following Pod in the manifest file:
```
kubectl create -f echoserver-pod.yaml
```

 - List current existing Pods
```
kubectl get pods
```

### Creating Our First ReplicaSet

 - Inspect the ReplicaSet's definition
```
cat echoserver-rs.yaml
```

 - List current existing ReplicaSets
```
kubectl get replicasets
```

 - Create the following ReplicaSet
```
kubectl create -f echoserver-rs.yaml
```

 - List current existing ReplicaSets
```
kubectl get rs
```

 - List current existing pods
```
kubectl get pods
```

### Scaling our application using ReplicaSets

 - Inspect the current ReplicaSet
```
kubectl describe rs echoserver
```

 - Edit the ReplicaSet to update "replicas" from "3" to "2"
```
kubectl edit rs echoserver
```

 - List current existing ReplicaSets
```
kubectl get rs
```

 - List current existing Pods
```
kubectl get pods
```

### Cleanup

 - List existing resources in your namespace
```
kubectl get all
```

 - Delete all existing resources
```
kubectl delete all --all
```

 - List existing resources to ensure that the environment is clean
```
kubectl get all
```
